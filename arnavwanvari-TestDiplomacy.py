import sys
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # 5 edge test cases

    # Test 1: When support armies are destroyed
    def test_1(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput
        
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Support C\nC London Move Madrid\n")
        diplomacy_solve(r)

        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), "A [dead]\nB [dead]\nC Madrid\n")

    # Test 2: When both armies switch cities
    def test_2(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput

        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        diplomacy_solve(r)
        
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), "A Barcelona\nB Madrid\n")

    # Test 3: When four armies are involved with two supporting the other two
    def test_3(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput
        
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Support A\nD Paris Support B\n")
        diplomacy_solve(r)
        
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), "A Barcelona\nB Madrid\nC London\nD Paris\n")

    # Test 4: When both armies hold their positions
    def test_4(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput
        
        r = StringIO("A Madrid Hold\nB Barcelona Hold\n")
        diplomacy_solve(r)

        self.assertEqual(capturedOutput.getvalue(), "A Madrid\nB Barcelona\n")

    # Test 5: When both armies move to different locations
    def test_5(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput
        
        r = StringIO("A Madrid Move Paris\nB Barcelona Move Rome\n")
        diplomacy_solve(r)
        
        self.assertEqual(capturedOutput.getvalue(), "A Paris\nB Rome\n")

    # Test 6: A complex situation with multiple supports and moves
    def test_6(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput
        
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Madrid\nE Rome Move Barcelona\nF Berlin Support E\n")
        diplomacy_solve(r)
        
        sys.stdout = sys.__stdout__
        # Assuming support from C to B is not disrupted and F supports E successfully:
        self.assertEqual(capturedOutput.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE [dead]\nF Berlin\n")

    # Test 7: Moving to a non-contested city
    def test_7(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput

        r = StringIO("A Madrid Move Valencia\nB Barcelona Move Seville\nC London Hold\n")
        diplomacy_solve(r)

        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), "A Valencia\nB Seville\nC London\n")

    # Test 8: Two supporting armies are attacked
    def test_8(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput
        
        r = StringIO("A Madrid Support B\nB Barcelona Support A\nC London Move Madrid\nD Paris Move Barcelona\n")
        diplomacy_solve(r)

        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()
